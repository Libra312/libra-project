// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from 'router'
import store from './store/index'
import date from 'utils/date'
import Cookie from 'utils/cookie'
import storage from 'utils/storage'
import {
  IMAGE_DOMAIN
} from 'configs'
import Progress from 'components/progress'
// filter引入
import 'filters'
import 'directives/ossImg'
import 'directives/policyUrl'

// element ui 引入
import {
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Radio,
  RadioGroup,
  Checkbox,
  CheckboxGroup,
  Input,
  Select,
  Option,
  Button,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Form,
  FormItem,
  Icon,
  Col,
  Row,
  Pagination,
  Upload,
  Dialog,
  Message,
  MessageBox,
  Tooltip,
  Loading,
  MenuItem,
  Menu,
  Submenu,
  MenuItemGroup,
  Alert,
  Step,
  Steps
} from 'element-ui'
// element ui 定制样式引入
import './assets/styles/theme/index.css'

require('es6-promise').polyfill()

Vue.use(Progress)
Vue.use(Dialog)
Vue.use(Dropdown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Input)
Vue.use(Select)
Vue.use(Option)
Vue.use(Button)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(DatePicker)
Vue.use(TimeSelect)
Vue.use(TimePicker)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Icon)
Vue.use(Row)
Vue.use(Col)
Vue.use(Pagination)
Vue.use(Upload)
Vue.use(Tooltip)
Vue.use(MenuItem)
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItemGroup)
Vue.use(Alert)
Vue.use(Step)
Vue.use(Steps)

Vue.directive('privilege', {
  bind: function (el, binding) {
    if (storage.getStorage('roles').indexOf(binding.value) < 0) {
      el.style.display = 'none'
    }
  }
})
Vue.prototype.$loading = Loading.service
Vue.prototype.$message = Message
Vue.prototype.$alert = (msg, type) => {
  Message({
    showClose: true,
    message: msg,
    type: type || 'success'
  })
}
Vue.prototype.$confirm = (msg, option) => {
  let op = option || {}
  MessageBox({
    confirmButtonText: op.doneBtn || '确定',
    cancelButtonText: op.failBtn || '取消',
    showCancelButton: op.showFailBtn || false,
    type: op.type || 'warning',
    title: op.title || '提示',
    message: msg,
    callback (action, instance) {
      if (action === 'confirm') {
        op.done && op.done()
      } else {
        op.fail && op.fail()
      }
    }
  })
}
Vue.prototype.$subNum = (a, num) => {
  var type = typeof (a)
  let arr = []
  if (type === 'number') {
    var str = a.toString()
    arr = str.split('.')
  } else if (type === 'string') {
    arr = a.split('.')
  } else {
    a = 0
  }
  if (arr.length > 1) {
    a = arr[0] + '.' + arr[1].substr(0, num)
  }
  return isNaN(a) ? 0 : +a
}

// 图片地址
Vue.prototype.$ImageDomin = IMAGE_DOMAIN

// 全局注册date方法
for (let i in date) {
  Vue.prototype['$' + i] = date[i]
}
// 全局注册cookie方法
for (let i in Cookie) {
  Vue.prototype['$' + i] = Cookie[i]
}
// 全局注册localStorage方法
for (let i in storage) {
  Vue.prototype['$' + i] = storage[i]
}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
