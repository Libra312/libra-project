// init
const infoMatch = window.location.hostname.split('.')

const APP_DOMAIN = 'u-bx.com'

// for env
let APP_ENV = infoMatch[0]

// for request prefix
let APP_REQUEST_PREFIX = `//${APP_ENV}.${APP_DOMAIN}/api`
// let APP_REQUEST_PREFIX = '//mp-jdp.com:8865/api'
// let APP_REQUEST_PREFIX = '//192.168.200.27:8865/api'
// let APP_REQUEST_PREFIX = '//192.168.200.102:8865/api'
// let APP_REQUEST_PREFIX = '//192.168.200.48:8865/api'
// let APP_REQUEST_PREFIX = '//192.168.200.70:8865/api'
// let APP_REQUEST_PREFIX = '//mp.baidu.com:8865/api'
let NO_API_PREFIX = `//${APP_ENV}.${APP_DOMAIN}`

let IMAGE_DOMAIN = `//img1.${APP_DOMAIN}`
// 电子保单域名
let POILCY_DOMAIN = `//epdf.${APP_DOMAIN}`
export {
  APP_ENV,
  APP_DOMAIN,
  APP_REQUEST_PREFIX,
  IMAGE_DOMAIN,
  POILCY_DOMAIN,
  NO_API_PREFIX
}
