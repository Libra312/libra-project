import Vue from 'vue'

Vue.directive('removeSpace', {
  bind: function (el, binding) {
    binding.replace(/\s+/g, '')
  }
})
