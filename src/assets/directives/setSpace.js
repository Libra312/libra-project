import Vue from 'vue'

Vue.directive('setSpace', {
  update: function (el, binding, vnode) {
    const data = binding.value || ''
    if (data) {
      const dto = data.dto
      const name = data.name
      const value = dto[name] || ''
      const replace = value.replace(/\s/g, '').replace(/(\S{4})/g, '$1 ')
      if (replace.length !== value.length) {
        // dto[name] = replace
        Vue.set(dto, name, replace)
      }
    }
  }
})
