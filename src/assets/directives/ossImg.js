import Vue from 'vue'
import { IMAGE_DOMAIN } from '../config'

const dealSrc = (value, { suffix }) => {
  if (!value) {
    return value
  }
  let isHttp = /^(?:[^\/]*)\/\/(.+)/.test(value) // eslint-disable-line
  let name = value.split('/data/image/')
  let path = name[1] ? name[1] : value
  let src = isHttp ? value : `${IMAGE_DOMAIN}/${path}${suffix}`
  return src
}

const srcBind = (el, binding, vnode) => {
  if (el.nodeName.toLowerCase() !== 'img') {
    return
  }
  const suffix = el.getAttribute('suffix') || ''
  el.src = dealSrc(binding.value, { suffix })
}

const directive = {
  bind: srcBind,
  componentUpdated: srcBind
}

Vue.directive('img-src', directive)
Vue.prototype.$dealSrc = dealSrc
