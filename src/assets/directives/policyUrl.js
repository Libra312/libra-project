import Vue from 'vue'
import { POILCY_DOMAIN } from '../config'

const dealSrc = (value) => {
  if (!value) {
    return value
  }
  let isHttp = /^(?:[^\/]*)\/\/(.+)/.test(value) // eslint-disable-line
  let src = isHttp ? value : `${POILCY_DOMAIN}/${value}`
  return src
}
const srcBind = (el, binding) => {
  if (el.nodeName.toLowerCase() !== 'a') {
    return
  }
  el.href = dealSrc(binding.value)
}

const directive = {
  bind: srcBind,
  componentUpdated: srcBind
}

Vue.directive('policy-src', directive)
