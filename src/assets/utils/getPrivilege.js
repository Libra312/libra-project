import Request from 'utils/request'
import storage from 'utils/storage'
import store from '../../store/index'
const getPrivilege = () => {
  // let privileges = storage.getStorage('roles')
  Request.post({
    url: `/manage/jurisdiction/getJurisdiction`,
    done: (res) => {
      if (res.resultCode === 1) {
        // 将获取到的最新数据添加到 localstorage
        storage.setStorage('roles', res.data)
        // 存储到 vuex 中
        store.commit('addRoles', res.data)
        // if (!privileges) window.location.reload()
      } else {
        console.log(res.message)
      }
    },
    fail(err) {
      console.log(err)
    }
  })
}

export default getPrivilege