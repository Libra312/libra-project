const BoolToNum = (bool) => {
  return bool ? 1 : 0
}

export default BoolToNum
