import { Message } from 'element-ui'

const copyUrl = (url, cb) => {
  try {
    var input = document.getElementById('copyInput')
    input.value = url
    input.focus()
    input.select()
    if (document.execCommand('copy', false, null)) {
      Message({
        showClose: true,
        message: '复制成功',
        type: 'success'
      })
    } else {
      Message({
        showClose: true,
        message: '当前浏览器不支持复制操作，请使用Ctrl+c手动复制',
        type: 'waring'
      })
    }
  } catch (e) {
    Message({
      showClose: true,
      message: '复制出错:' + JSON.stringify(e),
      type: 'error'
    })
  }
}
// 复制链接
export default copyUrl
