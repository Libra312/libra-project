import axios from 'axios'
import cookie from './cookie'
import {
  APP_ENV,
  APP_REQUEST_PREFIX
} from '../config'

export default {
  _checkStatus(status) {
    if (status === 401) {
      cookie.clearCookie()
      window.location.href = '/user/login'
      return
    } else if (status === 403) {
      cookie.clearCookie()
      window.location.href = '/powererror'
    }
    return this
  },

  _checkParams(params) {
    if (!params || !params.url) {
      throw new Error('request\'url is missed!')
    }
    return this
  },

  _getHeaders(header) {
    let headers = {}
    const userToken = cookie.getCookie('_youjia_token')

    if (userToken) {
      headers['Authorization'] = userToken
    }

    if (header) {
      for (let key in header) {
        headers[key] = header[key]
      }
    }

    return headers
  },

  _defaultParamsCallback(response, params) {
    response.then(function (res) {
      let data = res.data

      if (data && data.err && APP_ENV !== 'pro') {
        console && console.error(data.err)
      }
      params.done && params.done(res.data)
    }).catch(function (err) {
      if (!err) {
        params.fail && params.fail('can not catch error!')
      } else {
        if (err.response) {
          if (err.response.status === 401) {
            return response
          }
          params.fail && params.fail(err)
        } else if (err.message) {
          params.fail && params.fail(err)
        }
      }
    })
    return response
  },

  _request({
    url,
    method,
    params = {},
    data = {},
    baseURL,
    header
  }) {
    const headers = this._getHeaders(header)

    return axios.request({
      method,
      baseURL,
      url,
      params,
      data,
      headers,
      validateStatus: (status) => {
        this._checkStatus(status)
        return status >= 200 && status <= 500 && status !== 404 && status !== 401
      }
    })
  },

  post(params) {
    const res = this._checkParams(params)._request({
      method: 'post',
      url: params.url,
      data: params.data,
      header: params.header,
      baseURL: APP_REQUEST_PREFIX
    })

    return this._defaultParamsCallback(res, params)
  },

  get(params, noArea) {
    const res = this._checkParams(params)._request({
      method: 'get',
      url: params.url,
      params: params.data,
      header: params.header,
      baseURL: APP_REQUEST_PREFIX
    })

    return this._defaultParamsCallback(res, params)
  },

  checkLogin() {
    return !cookie.getCookie('_youjia_token') && this._checkStatus(401)
  }
}
