import Request from 'utils/request'
import storage from 'utils/storage'

const flatArray = (arr) => {
  let obj = {}
  let filterArray = []
  for (let i = 0; i < arr.length; i++) {
    const ins = arr[i]
    for (let j = 0; j < ins.Areas.length; j++) {
      // const city = ins.Areas[j]
      let city = {
        code: ins.Areas[j].CityCode,
        name: ins.Areas[j].CityName,
        province: ins.Areas[j].ProvinceName,
        provinceCode: ins.Areas[j].ProvinceCode,
        licensePlate: ins.Areas[j].VehicleLicenceCode
      }
      const code = city.code
      // const index = filterArray.indexOf(code)
      if (obj[code]) {
        obj[code].ins.push({
          code: ins.SupplierId,
          name: ins.SupplierName
        })
      } else {
        filterArray.push(code)
        city.ins = [{
          code: ins.SupplierId,
          name: ins.SupplierName
        }]
        obj[code] = city
      }
    }
  }
  return obj
}

const getInsCompanyCitys = () => {
  let citys = storage.getStorage('youjia_citys')
  return Request.get({
    url: `/order/insCompanyCitys`,
    done: (res) => {
      if (res.resultCode === 1) {
        storage.setStorage('youjia_citys', flatArray(res.dtoList))
        if (!citys) window.location.reload()
      } else {
        console.log(res.message)
      }
    },
    fail(err) {
      console.log(err)
    }
  })
}

export default getInsCompanyCitys
