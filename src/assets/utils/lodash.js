const _filter = (array, cb) => {
  let arr = []
  for (let i = 0; i < array.length; i++) {
    const item = array[i]
    if (cb(item)) {
      arr.push(item)
    }
  }
  return arr
}
export { _filter }