import Vue from 'vue'

// Vue.filter('bool', function (val) {
//   return val ? true : false
// })

Vue.filter('boolCN', function (val) {
  return val ? '是' : '否'
})
Vue.filter('fillDecimal', function (val) {
  if (val < 0) {
    return 0
  }
  var str = String(val)
  var str2 = str.split('.', 2)
  if (Number(str2[1])) {
    var str3 = str2[1].slice(0, 1)
    var str4 = str2[0] + '.' + str3
  } else {
    return str2[0]
  }
  return str4
})
