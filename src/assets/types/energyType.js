const energyType = {
  '0': '燃油',
  '1': '纯动力',
  '2': '燃料电池',
  '3': '插电式混合动力',
  '4': '其他混合动力'
}
// 动力类型code
export default energyType
