
// const status = {
//   '1': '超时取消',
//   '2': '待支付',
//   '3': '待返佣',
//   '4': '待审核',
//   '5': '审核失败',
//   '6': '返佣完成',
//   '7': '已完成',
//   '8': '作废',
//   '9': '支付失败',
//   '10': '返佣失败',
//   '11': '报价失败',
//   '12': '待核保',
//   '13': '退回',
//   '14': '人工待核保',
//   '15': '人工退回',
//   '16': '核保失败',
//   '17': '核保退回',
//   '18': '核保中',
//   '19': '结算失败',
//   '20': '结算成功'
// }
// // 订单状态码
// export default status
const status = {
  '10': '暂存',
  '11': '报价失败',
  '20': '核保中',
  '21': '核保失败',
  '30': '核保通过',
  '31': '核保退回',
  '40': '待支付',
  '41': '申请支付失败',
  '42': '申请支付成功',
  '50': '待审核',
  '51': '支付失败',
  '60': '审核中',
  '61': '出单失败',
  '70': '审核成功',
  '71': '审核拒绝',
  '80': '已完成',
  '81': '结算失败',
  '100': '超时取消'
}
// 订单状态码
export default status
