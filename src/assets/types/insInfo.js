const businessDtoList = [
  {
    isOn: '0',
    category: '2',
    code: '10001',
    needCode: '30001',
    needCheck: false,
    name: '车损险',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  },
  {
    isOn: '0',
    category: '2',
    code: '10002',
    needCode: '30002',
    needCheck: false,
    name: '三者险',
    data: [
      {
        'val': '0',
        'txt': '不投保'
      },
      {
        'val': '50000',
        'txt': '5万'
      },
      {
        'val': '100000',
        'txt': '10万'
      },
      {
        'val': '150000',
        'txt': '15万'
      },
      {
        'val': '200000',
        'txt': '20万'
      },
      {
        'val': '300000',
        'txt': '30万'
      },
      {
        'val': '500000',
        'txt': '50万'
      },
      {
        'val': '1000000',
        'txt': '100万'
      },
      {
        'val': '2000000',
        'txt': '200万'
      }]
  },
  {
    isOn: '0',
    category: '2',
    code: '10003',
    needCode: '30003',
    needCheck: false,
    name: '盗抢险',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  },
  {
    isOn: '0',
    category: '2',
    code: '10004',
    needCode: '30004',
    needCheck: false,
    name: '司机责任险',
    data: [{
      'val': '0',
      'txt': '不投保'
    }, {
      'val': '10000',
      'txt': '1万/座'
    },
    {
      'val': '20000',
      'txt': '2万/座'
    },
    {
      'val': '30000',
      'txt': '3万/座'
    },
    {
      'val': '40000',
      'txt': '4万/座'
    },
    {
      'val': '50000',
      'txt': '5万/座'
    },
    {
      'val': '100000',
      'txt': '10万/座'
    }]
  },
  {
    isOn: '0',
    category: '2',
    code: '10005',
    needCode: '30005',
    needCheck: false,
    name: '乘客责任险',
    data: [{
      'val': '0',
      'txt': '不投保'
    }, {
      'val': '10000',
      'txt': '1万/座'
    },
    {
      'val': '20000',
      'txt': '2万/座'
    },
    {
      'val': '30000',
      'txt': '3万/座'
    },
    {
      'val': '40000',
      'txt': '4万/座'
    },
    {
      'val': '50000',
      'txt': '5万/座'
    },
    {
      'val': '100000',
      'txt': '10万/座'
    }]
  }
]

const otherDtoList = [
  {
    isOn: '0',
    category: '2',
    code: '20201',
    name: '玻璃险',
    data: [{
      'val': '0',
      'txt': '不投保'
    }, {
      'val': '1',
      'txt': '国产'
    }, {
      'val': '2',
      'txt': '进口'
    }]
  },
  {
    isOn: '0',
    category: '2',
    code: '20202',
    needCode: '30202',
    needCheck: false,
    name: '划痕险',
    data: [{
      'val': '0',
      'txt': '不投保'
    }, {
      'val': '2000',
      'txt': '2000'
    },
    {
      'val': '5000',
      'txt': '5000'
    },
    {
      'val': '10000',
      'txt': '10000'
    },
    {
      'val': '20000',
      'txt': '20000'
    }]
  },
  {
    isOn: '0',
    category: '2',
    code: '20203',
    needCode: '30203',
    needCheck: false,
    name: '自燃险',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  },
  {
    isOn: '0',
    category: '2',
    code: '20204',
    needCode: '30204',
    needCheck: false,
    name: '涉水险',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  },
  {
    isOn: '0',
    category: '3',
    code: '20205',
    name: '指定专修险',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  },
  {
    isOn: '0',
    category: '3',
    code: '20210',
    name: '无法找到第三方',
    data: [{ val: '0', txt: '不投保' }, { val: '1', txt: '投保' }]
  }
]

export {
  otherDtoList,
  businessDtoList
}
