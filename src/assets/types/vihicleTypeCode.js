const vehicleType = {
  'vehicleLicenceNo': '202',
  'engineNo': '204',
  'vhlFrameNo': '201',
  'autoModelName': '203',
  'firstRegDate': '205',
  'issueDate': '206',
  'seats': '207',
  'exhaustCapability': '208',
  'vehicleTonnages': '209',
  'wholeWeight': '210',
  'transferCarFlag': '200'
}
const insuranceMess = {
  '1': '投保',
  '801': '交强险退保-报废',
  '802': '交强险退保-报停',
  '803': '交强险退保-转籍',
  '804': '交强险退保-丢失',
  '805': '交强险退保-新车退回返厂',
  '806': '交强险退保-重复投保'
}

const genera = {
  'riskChangeList': '保额及险别变更',
  'vhlInfoChangeList': '车辆信息变更',
  'personChangeList': '人员变更'
}

const insuranceKey = ['801', '802', '803', '804', '805', '806']
const rederKey1 = ['10001', '10002', '10003', '10004', '10005', '20202', '20203', '20204', '20205', '20210', '20212', '810']
const rederKey2 = {
  '0': '不投保',
  '1': '国产',
  '2': '进口'
}
const vehicleName = {
  'vehicleLicenceNo': '车牌号',
  'engineNo': '发动机型号',
  'vhlFrameNo': '车架号',
  'autoModelName': '品牌型号',
  'firstRegDate': '初登日期',
  'issueDate': '发证日期',
  'seats': '座位数',
  'exhaustCapability': '排量',
  'vehicleTonnages': '核定载质量',
  'wholeWeight': '整备质量',
  'transferCarFlag': '是否过户'
}

const personListCode = [
  {
    title: '投保人',
    name: '',
    nameCode: '310',
    customsCode: '311',
    customs: '',
    adressCode: '312',
    adress: ''
  },
  {
    title: '被保人',
    nameCode: '300',
    customsCode: '301',
    name: '',
    customs: '',
    adressCode: '302',
    adress: ''
  },
  {
    title: '车主',
    customs: '',
    name: '',
    nameCode: '320',
    customsCode: '321',
    adress: '',
    adressCode: '322'
  }
]

const personListType = {
  '300': 'insuredName',
  '301': 'insuredCertNo',
  '302': 'insuredAddr',
  '310': 'applyInsName',
  '311': 'applyInsCertNo',
  '312': 'applyInsAddr',
  '320': 'vhlOwnerName',
  '321': 'vhlOwnerCertNo',
  '322': 'vhlOwnerAddr'
}
const personListName = {
  '300': '被保人姓名',
  '301': '被保人证件号',
  '302': '被保人地址',
  '310': '投保人姓名',
  '311': '投保人证件号',
  '312': '投保人地址',
  '320': '车主姓名',
  '321': '车主证件号',
  '322': '车主地址'
}
const endorStatus = {
  '1': '待审核',
  '2': '已打回',
  '3': '处理中',
  '4': '审核未通过',
  '5': '已作废',
  '6': '审核通过',
  '7': '未支付',
  '8': '已完成',
  '21': '已支付',
  '22': '支付超时'
}
export {vehicleType, insuranceMess, insuranceKey, genera, rederKey1, rederKey2, vehicleName, personListCode, personListType, personListName, endorStatus}
