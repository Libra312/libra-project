const taxType = {
  '1': '交税',
  '2': '完税车',
  '4': '免税车',
  '8': '减税车',
  '16': '不交税'
}
// 车船税缴税类型
export default taxType
