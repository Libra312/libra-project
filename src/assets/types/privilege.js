const pathObj = {
  '/order': 'E001',
  '/insure/review/Order': 'E101',
  '/endor/order': 'F001',
  '/policy/batch': 'G001',
  // TODO
  '/operation/redpacketlist': 'R102',
  '/operation/createredpacket': 'R101',
  '/upay/trade/remit/upload': 'Z101',
  '/upay/capital/record': 'L101',
  '/upay/capital/capitalFlow': 'L102',
  '/account/wallet': 'H102',
  '/account/order': 'H101',
  '/account/personnels': 'H103',
  '/accmanage/personnels': 'H103'
}

export default pathObj
