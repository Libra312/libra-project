const imageTypes = {
  '0': '被保人身份证正面照',
  '1': '被保人身份证反面照',
  '2': '被保人组织机构代码证照',
  '3': '行驶证正页照',
  '4': '行驶证副页照',
  '5': '车辆正面照片',
  '6': '车辆正后照片',
  '7': '车辆前左45度照片',
  '8': '车辆前右45度照片',
  '9': '车辆后左45度照片',
  '10': '车辆后右45度照片',
  '11': '带车架号照片',
  '12': '铭牌',
  '13': '发动机舱',
  '14': '新车发票照',
  '15': '本地身份证照',
  '16': '上年商业险保单照',
  '17': '上年交强险保单照',
  '18': '行驶证年审页照',
  '19': '合格证',
  '20': '驾驶证正页照',
  '21': '驾驶证副页照',
  '22': '关单',
  '23': '数字一致性证书第二页',
  '24': '车主身份证反面照',
  '25': '车主组织机构代码证照',
  '26': '投保人身份证正面照',
  '27': '投保人身份证反面照',
  '28': '投保人组织机构代码证',
  '29': '左车仪表盘内饰',
  '30': '右车仪表盘内饰',
  '31': '前挡风玻璃',
  '32': '玻璃标识',
  '33': '打着火拍仪表盘',
  '34': '倒车镜前',
  '35': '倒车镜后',
  '36': '所有大灯特写一',
  '37': '所有大灯特写二',
  '38': '所有大灯特写三',
  '39': '所有大灯特写四',
  '40': '供应商logo',
  '41': '其他资料1',
  '42': '其他资料2',
  '43': '其他资料3',
  '44': '银行卡正面照',
  '45': '资格证正面照',
  '46': '居住证',
  '47': '浮动告知书盖章单位车',
  '48': '投保单盖公章照',
  '49': '二手车交易发票',
  '50': '交强险不退保承诺书, 上年理赔记录',
  '51': '本地使用证明',
  '52': '完税证明',
  '53': '被保人社会信用代码证照',
  '54': '投保人社会信用代码证照',
  '55': '车主社会信用代码证'
}
// 图片类型code
export default imageTypes
