import idType from './idType'
import taxType from './taxType'
import imageType from './imageType'
import energyType from './energyType'
import carProperty from './carProperty'
import insCompanyCode from './insCompanyCode'
import insProvinceCode from './insProvinceCode'
import vehicleClassCode from './vehicleClassCode'
import vehiclePropertyCode from './vehiclePropertyCode'
import vehicleClassCodeList from './vehicleClassCodeList'
import promotionType from './promotionType'
import orderStatu from './orderStatu'

import insStatus from './insStatus'
import regYear from './regYear'

export {
  insStatus,
  regYear,
  idType,
  taxType,
  imageType,
  energyType,
  carProperty,
  insCompanyCode,
  insProvinceCode,
  vehicleClassCode,
  vehiclePropertyCode,
  vehicleClassCodeList,
  promotionType,
  orderStatu
}
