const orderStatu = {
  '50': '已打款',
  '40': '打款中',
  '20': '待打款',
  '61': '打款失败',
  '10': '已校验',
  '21': '已取消',
  '11': '校验失败'
}
// 批次状态
export default orderStatu
