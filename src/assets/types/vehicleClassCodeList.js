const codeArr = [
  { code: '1',
    name: '客车',
    seats: true,
    exhaust: true,
    list: [
      {code: '101', name: '私家车'},
      {code: '102', name: '非营运客车'},
      {code: '103', name: '公共租赁'},
      {code: '104', name: '城市交通'},
      {code: '105', name: '公路客运'},
      {code: '106', name: '旅游客运'}
    ]
  },
  { code: '2',
    name: '货车',
    seats: true,
    ton: true,
    list: [
      {code: '201', name: '普通货车'},
      {code: '202', name: '自卸货车'},
      {code: '203', name: '货车(半挂)'},
      {code: '204', name: '三轮汽车'},
      {code: '205', name: '低速载货'},
      {code: '206', name: '厢式货车'}
    ]
  },
  { code: '3',
    name: '挂车',
    seats: true,
    ton: true,
    list: [
      {code: '301', name: '货车(半挂)'},
      {code: '302', name: '特一挂车(油汽液)'},
      {code: '303', name: '特二挂车(罐式)'},
      {code: '304', name: '特二挂车(冷藏，保温)'},
      {code: '305', name: '特三挂车(仪器设备)'}
    ]
  },
  { code: '8',
    name: '摩托车',
    power: true,
    list: [
      {code: '801', name: '排量量50CC以下'},
      {code: '802', name: '排量量50CC-250CC(含)'},
      {code: '803', name: '排量量250CC以上及三轮摩托'}
    ]
  },
  { code: '9',
    name: '拖拉机、农用车',
    seats: true,
    exhaust: true,
    ton: true,
    power: true,
    list: [
      {code: '901', name: '运输型拖拉机'},
      {code: '902', name: '兼用型拖拉机'},
      {code: '903', name: '联合收割机'},
      {code: '904', name: '⽔利机械'},
      {code: '905', name: '割晒机'},
      {code: '906', name: '牧草 (⻘青贮)收获机'},
      {code: '907', name: '机动脱⾕谷机'},
      {code: '908', name: '机动插秧机'},
      {code: '909', name: '喷灌机'},
      {code: '910', name: '场院机械'}
    ]
  },
  { code: '21',
    name: '特一',
    seats: true,
    ton: true,
    list: [
      {code: '2101', name: '油罐车'},
      {code: '2102', name: '气罐车'},
      {code: '2103', name: '液罐车'}
    ]
  },
  { code: '22',
    name: '特二',
    seats: true,
    exhaust: true,
    ton: true,
    power: true,
    list: [
      {code: '2201', name: '清障⻋'},
      {code: '2202', name: '清扫⻋'},
      {code: '2203', name: '清洁⻋'},
      {code: '2204', name: '起重⻋'},
      {code: '2205', name: '装卸⻋'},
      {code: '2206', name: '升降⻋'},
      {code: '2207', name: '搅拌车'},
      {code: '2208', name: '挖掘机'},
      {code: '2209', name: '推⼟车'},
      {code: '2210', name: '混凝⼟泵车'},
      {code: '2211', name: '压路车'},
      {code: '2212', name: '矿山车'},
      {code: '2213', name: '轮式工程车'},
      {code: '2214', name: '保温车'},
      {code: '2215', name: '冷藏车'},
      {code: '2216', name: '特⼆挂⻋(罐式)'},
      {code: '2217', name: '特⼆挂⻋(冷藏、保温)'},
      {code: '2218', name: '净水车'},
      {code: '2219', name: '特二其他'}
    ]
  },
  { code: '23',
    name: '特三',
    seats: true,
    exhaust: true,
    ton: true,
    power: true,
    list: [
      {code: '2301', name: '电视转播⻋'},
      {code: '2302', name: '消防车'},
      {code: '2303', name: '医疗车'},
      {code: '2304', name: '救护车'},
      {code: '2305', name: '监测车'},
      {code: '2306', name: '雷达车'},
      {code: '2307', name: 'X光检查⻋'},
      {code: '2308', name: '电信⼯程车'},
      {code: '2309', name: '电力⼯程车'},
      {code: '2310', name: '专业拖车'},
      {code: '2311', name: '运钞车'},
      {code: '2312', name: '邮电车'},
      {code: '2313', name: '警用车'},
      {code: '2314', name: '特三挂车(仪器器设备)'},
      {code: '2316', name: '特二其他'}
    ]
  },
  { code: '24',
    name: '特四',
    seats: true,
    exhaust: true,
    ton: true,
    power: true,
    list: [
      {code: '2401', name: '集装箱拖头'}
    ]
  },
  { code: '99',
    name: '其他',
    seats: true,
    exhaust: true,
    ton: true,
    power: true
  }
]
// 车辆类别
export default codeArr
