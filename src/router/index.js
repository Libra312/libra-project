import Vue from 'vue'
import Router from 'vue-router'

import Request from 'utils/request'
import Privilege from '../assets/types/privilege'
import storage from 'utils/storage'

import policy from 'pages/policy'
import policyBatch from 'pages/policy/batch'
import policyApply from 'pages/policy/apply'

import Order from 'pages/order'
import Print from 'pages/print'
import Endor from 'pages/endor'
import EndorOrder from 'pages/endor/order'
import EndorReview from 'pages/endor/review'

import Pay from 'pages/pay'
import PayList from 'pages/pay/list'
import PayDetail from 'pages/pay/detail'

import Insure from 'pages/insure'
import InsureReview from 'pages/insure/review'

import User from 'pages/user'
import UserLogin from 'pages/user/login'
import UserApply from 'pages/user/apply'
import UserForget from 'pages/user/forget'
import UserRegister from 'pages/user/register'

import Account from 'pages/account'
import AccountWallet from 'pages/account/wallet'
import AccountWalletHome from 'pages/account/wallet/home'
import AccountWalletBank from 'pages/account/wallet/bank'
import AccountWalletWithdraw from 'pages/account/wallet/withdraw'
import AccountSetting from 'pages/account/setting'
import AccountPersonnels from 'pages/account/personnels'
import PowerError from 'pages/powerError'
import AccountJobNo from 'pages/account/jobno'
import AccountOrder from 'pages/account/order'
import Error from 'pages/error'

import Operation from 'pages/operation'
import RedPacketList from 'pages/operation/redpacketlist'
import CreateRedPacket from 'pages/operation/createredpacket'
import uPay from 'pages/upay'
import uPayCapital from 'pages/upay/capital'
import uPaycapitalFlow from 'pages/upay/capital/capitalFlow'
import uPayrechargeRecord from 'pages/upay/capital/rechargeRecord'
import uPayTrade from 'pages/upay/trade'
import uPayRemit from 'pages/upay/trade/remit'
import uPayRemitList from 'pages/upay/trade/remitList'
import uPayTradeDetail from 'pages/upay/trade/tradeDetail'
import uPayLock from 'pages/upay/trade/remit/lock'
import uPayStart from 'pages/upay/trade/remit/start'
import uPayLoad from 'pages/upay/trade/remit/upload'

import AccManage from 'pages/accmanage'
// import AccManageSetting from 'pages/accmanage/setting'
// import AccManagePersonnels from 'pages/accmanage/personnels'

Vue.use(Router)

let ROUTER_ENTER_TIME = 0
let HASH = location.hash
let roles = storage.getStorage('roles') || []
// 登录之后自动跳到第一个有权限的界面
let redirect = () => {
  let res = []
  let pri = roles || router.app.$getStorage('roles')
  // console.log(pri)
  for (const key in Privilege) {
    const element = Privilege[key]
    if (pri.indexOf(element) > -1) {
      res.push(key)
    }
    // console.log(res)
  }
  return res[0]
}

let router = new Router({
  mode: HASH === '' ? 'history' : 'hash',
  linkActiveClass: 'active',
  routes: [{
    path: '/',
    name: 'home'
  }, {
    path: '/print/:id',
    name: 'print',
    component: Print
  }, {
    path: '/order',
    name: 'order',
    component: Order
  }, {
    path: '/pay',
    name: 'pay',
    component: Pay,
    redirect: '/pay/list',
    children: [{
      path: 'list',
      name: 'PayList',
      component: PayList
    }, {
      path: 'detail',
      name: 'PayDetail',
      component: PayDetail
    }, {
      path: 'detail/:id',
      name: 'PayDetailId',
      component: PayDetail
    }]
  }, {
    path: '/insure',
    name: 'insure',
    component: Insure,
    redirect: '/order',
    children: [{
      path: 'verify/:id',
      name: 'verify',
      component: InsureReview
    }, {
      path: 'review/:id',
      name: 'review',
      component: InsureReview
    }]
  }, {
    path: '/policy',
    name: 'policy ',
    component: policy,
    redirect: '/policy/batch',
    children: [{
      path: 'batch',
      name: 'batch',
      component: policyBatch
    }, {
      path: 'apply/:id',
      name: 'apply',
      component: policyApply
    }, {
      path: 'edit/:id',
      name: 'policyEdit',
      component: policyApply
    }, {
      path: 'copy/:id',
      name: 'policyCopy',
      component: policyApply
    }]
  }, {
    path: '/account',
    name: 'account',
    component: Account,
    redirect: '/account/personnels',
    children: [{
      path: 'wallet',
      component: AccountWallet,
      children: [{
        path: '',
        name: 'wallet-home',
        component: AccountWalletHome
      }, {
        path: 'bank',
        name: 'wallet-bank',
        component: AccountWalletBank
      }, {
        path: 'withdraw',
        name: 'withdraw',
        component: AccountWalletWithdraw
      }]
    }, {
      path: 'setting',
      name: 'setting',
      component: AccountSetting
    }, {
      path: 'personnels',
      name: 'personnels',
      component: AccountPersonnels
    }, {
      path: 'jobno',
      name: 'jobno',
      component: AccountJobNo
    }, {
      path: 'order',
      name: 'accountOrder',
      component: AccountOrder
    }]
  }, {
    path: '/user',
    name: 'user',
    component: User,
    redirect: '/user/login',
    children: [{
      path: 'login',
      component: UserLogin,
      meta: {
        user: true
      }
    }, {
      path: 'apply',
      component: UserApply,
      meta: {
        user: true
      }
    }, {
      path: 'forget',
      component: UserForget,
      meta: {
        user: true
      }
    }, {
      path: 'register',
      component: UserRegister,
      meta: {
        user: true
      }
    }]
  }, {
    path: '/endor',
    name: 'endor',
    component: Endor,
    redirect: '/endor/order',
    children: [{
      path: 'review/:id',
      name: 'endorReview',
      component: EndorReview
    }, {
      path: 'order',
      name: 'endorOrder',
      component: EndorOrder
    }]
  }, {
    path: '/powererror',
    name: 'powererror',
    component: PowerError
  }, {
    path: '*',
    name: 'error',
    component: Error
  }, {
    path: '/operation',
    name: 'operation',
    component: Operation,
    redirect: roles.indexOf('R101') > -1 ? '/operation/createredpacket' : '/operation/redpacketlist',
    children: [{
      path: 'redpacketlist',
      name: 'redpacketlist',
      component: RedPacketList
    },
    {
      path: 'createredpacket',
      name: 'createredpacket',
      component: CreateRedPacket
    }]
  }, {
    path: '/upay',
    name: 'uPay',
    component: uPay,
    redirect: '/upay/trade/remit',
    children: [{
      path: 'capital',
      component: uPayCapital,
      name: 'Capital',
      redirect: '/upay/capital/capitalflow',
      children: [{
        path: 'capitalflow',
        name: 'capitalFlow',
        component: uPaycapitalFlow
      }, {
        path: 'record',
        name: 'record',
        component: uPayrechargeRecord
      }]
    }, {
      path: 'trade',
      name: 'Trade',
      component: uPayTrade,
      redirect: '/upay/trade/remit',
      children: [{
        path: 'remit',
        name: 'remit',
        component: uPayRemit,
        redirect: '/upay/trade/remit/upload',
        children: [{
          path: 'lock',
          name: 'lock',
          component: uPayLock
        }, {
          path: 'start',
          name: 'start',
          component: uPayStart
        }, {
          path: 'upload',
          name: 'upload',
          component: uPayLoad
        }]
      }, {
        path: 'remitlist',
        name: 'remitList',
        component: uPayRemitList
      }, {
        path: 'tradetail',
        name: 'traDetail',
        component: uPayTradeDetail
      }]
    }]
  }, {
    path: '/accmanage',
    name: 'accmanage',
    component: AccManage,
    redirect: '/accmanage/setting',
    children: [{
      path: 'setting',
      name: 'setting',
      component: AccountSetting
    }, {
      path: 'personnels',
      name: 'personnels',
      component: AccountPersonnels
    }]
  }]
})

// 全局控制路由，验证用户权限
router.beforeEach((to, from, next) => {
  const _this = router.app
  const token = _this.$getCookie('_youjia_token')
  // 避免 iOS 的 Safari 刷新页面出现两个进度条
  if (ROUTER_ENTER_TIME !== 0) {
    Vue.prototype.$progress.start(20)
  }
  ROUTER_ENTER_TIME++
  // 直接访问 '/pay' 路由和访问'/user'路由且没有登录的情况下
  // 直接next，不需要重定向到登录页面
  if (to.path.indexOf('user') > -1 || to.path.indexOf('powererror') > -1) {
    next()
    return
  }
  // 判断用户退出
  if (to.path.indexOf('logout') > -1) {
    _this.$clearCookie()
    // _this.$clearStorage()
    router.push('/user/login')
    return
  }
  // 判断是否登录
  if (token) {
    if (to.path.indexOf('user') > -1) {
      router.push('/')
    } else {
      let roles = _this.$getStorage('roles')
      if (!roles || (roles && roles.length === 0)) {
        Request.post({
          url: '/manage/jurisdiction/getJurisdiction',
          done: (res) => {
            // 存储权限信息到 vuex 和 localstorage 中
            _this.$store.commit('addRoles', res.data)
            _this.$setStorage('roles', JSON.parse(JSON.stringify(res.data)))
          },
          fail: (err) => {
            console.log(err)
            // 完全没有权限  直接登出
            router.push('/logout')
          }
        })
      } else {
        for (const key in Privilege) {
          if (to.path === key && roles.indexOf(Privilege[key]) < 0) {
            router.push('/powerError')
          }
        }
        if (to.path === '/') {
          router.push(redirect())
          return
        }
      }
      // router.push(a())
      next()
    }
  } else {
    Vue.prototype.$progress.end()
    window.location.href = '/user/login'
  }
})

// 控制路由进度条结束
router.afterEach((route) => {
  if (ROUTER_ENTER_TIME !== 1) {
    Vue.prototype.$progress.end()
  }
})
export default router
