// http://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    // 支持es6
    ecmaVersion: 6,
    sourceType: 'module'
  },
  env: {
    // 预定义es6环境的全局变量
    es6: true,
    // 预定义node环境的全局变量
    node: true,
    // 预定义浏览器环境的全局变量
    browser: true
  },
  // 引用 standard 风格的依赖包
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
