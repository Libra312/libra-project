# 有家

> 有家商铺项目

## 编译

``` bash
# 安装依赖
npm install

# 启动 3003 端口的服务 （后台运行）
npm run nohup

# 启动 3005 端口的服务
npm run dev

# 编译可执行静态文件
npm run build

# 上传静态文件到OSS
npm run upload

# 编译静态文件并上传到OSS
npm start

```

## 运行

浏览器打开： [127.0.0.1:3005](http://127.0.0.1:3005)

## Project Structure
```bash
.
├── build/                          # webpack 编译
│   ├── configs/                    # webpack 配置文件
│   ├── deploy/                     # 上传文件到阿里云
│   └── ...
├── dist/                           # webpack 编译文件目录
│   └── ...
├── src/                            # 文件主目录
│   ├── assets/                     # 公共文件
│   │   ├── config/                 # APP_ENV配置，域名配置
│   │   |   └── ...
│   │   ├── directives/             # vue 指令
│   │   |   └── ...
│   │   ├── filters/                # vue filter
│   │   |   └── ...
│   │   ├── styles/                 # css 公共文件
│   │   |   ├── theme/              # element ui 定制主题
│   │   |   └── ...
│   │   ├── types/                  # 常用类型枚举
│   │   |   └── ...
│   │   └── utils/                  # 常用工具脚本
│   │       └── ...
│   ├── components/                 # 组件目录
│   │   ├── footer/                 # 页脚组件
│   │   |   └── ...
│   │   ├── head-title/             # 头部标题组件
│   │   |   └── ...
│   │   ├── header/                 # 页头组件
│   │   |   └── ...
│   │   ├── progress/               # progress组件
│   │   |   └── ...
│   │   ├── public/                 # 公共组件
│   │   |   └── ...
│   │   ├── search/                 # search组件
│   │   |   └── ...
│   ├── pages/                      # 页面目录
│   │   ├── account/                # 账户相关页面
│   │   |   ├── jobno               # picc工号管理（弃用）
│   │   |   ├── model
│   │   |   │   ├── caretBank       # 添加银行卡
│   │   |   │   ├── payPassword     # 设置支付密码
│   │   │   │   └── ...
│   │   |   ├── order               # 订单
│   │   |   ├── personnels          # 子账号
│   │   |   ├── setting             # 设置
│   │   |   ├── wallet              # 钱包
│   │   │   └── ...
│   │   ├── endor/                  # 批单相关页面
│   │   |   ├── order               # 批单列表页面
│   │   │   │   └── ...
│   │   |   ├── review              # 批单详情页面
│   │   │   │   └── ...
│   │   ├── insure/                 # 订单页面
│   │   |   ├── review              # 订单详情页面
│   │   │   │   └── ...
│   │   ├── order/                  # 订单列表页面
│   │   |   └── ...
│   │   ├── pay/                    # 人保支付单相关页面（暂时弃用）
│   │   |   ├── detail
│   │   │   │   └── ...
│   │   |   ├── list
│   │   │   │   └── ...
│   │   ├── policy/                 # 政策单相关页面
│   │   |   ├── apply               # 创建订单
│   │   │   │   └── ...
│   │   │   │
│   │   │   ├── batch               # 工号与批次维护
│   │   │   │    ├── createBatch    # 添加批次
│   │   │   │    ├── createJobNo    # 添加工号
│   │   │   │    ├── renbaoCreate   # 添加人保政策
│   │   │   │    └── ...
│   │   │   └── ...
│   │   │
│   │   ├── user/                   # 用户相关页面
│   │   │   ├── login               # 登录页面
│   │   │   ├── apply               # 申请商户（暂时弃用）
│   │   │   ├── forget              # 重置密码（暂时弃用）
│   │   │   ├── register            # 注册（暂时弃用）
│   │   |   └── ...
│   │   ├── endor/                  # 批单管理
│   │   │
│   │   ├── error.vue               # 404页面
│   │   └── support.vue             # 浏览器支持页面
│   ├── router/                     # 路由目录
│   │   └── index.js
│   ├── store/                      # vuex 目录
│   │   └── index.js
│   ├── main.js                     # 入口文件
│   └── App.vue                     # 主组件
├── stats/                          # 打包模块分析
│   └── index.html
├── .babelrc                        # babel 设置
├── .editorconfig                   # 编辑器统一配置：缩进，空格/制表符
├── .eslintrc.js                    # eslint 配置
├── .eslintignore                   # eslint 排除规则
├── .gitignore                      # git排除目录规则
├── .postcssrc.js                   # postcss 配置
├── index.ejs                       # 首页模版
├── package-lock.json               # npm模块锁
├── package.json                    # package，npm配置
├── README.md                       # 说明文档
└── yarn.lock                       # yarn模块锁
```
